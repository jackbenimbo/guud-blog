require 'test_helper'

class BlogPagesControllerTest < ActionController::TestCase
  test "should get home" do
    get :home
    assert_response :success
    assert_select "title", "Home | Guudvibes"
  end

  test "should get about" do
    get :about
    assert_response :success
    assert_select "title", "About | Guudvibes"
  end

  test "should get goodstuff" do
    get :goodstuff
    assert_response :success
    assert_select "title", "Goodstuff | Guudvibes"
  end
  
    test "should get serenity" do
    get :serenity
    assert_response :success
    assert_select "title", "Serenity | Guudvibes"
  end
  
  test "should get contact" do
    get :contact
    assert_response :success
    assert_select "title", "Contact | GuudVibes"
  end


end
