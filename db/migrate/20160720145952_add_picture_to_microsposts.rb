class AddPictureToMicrosposts < ActiveRecord::Migration
  def change
    add_column :microsposts, :picture, :string
  end
end
